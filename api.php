 Route::post('upload', function (Request $request) {
        $request->validate([

            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:20000',
        ]);
        if (isset($request->photo)) {

            $path = Storage::disk('public')->putFile( 'test', $request->photo );
            return  ['location' => 'storage/' . $path] ;
        }
    });