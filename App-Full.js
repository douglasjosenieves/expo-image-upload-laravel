import React,{useState} from 'react';
import { Text, View, ScrollView } from 'react-native';
import UploadImage from './UploadImage';

function App ({
    params,
}) {

  const [image, setImage] = useState('');
  const [image2, setImage2] = useState('');

  const imageUrl = (url) => {
    setImage(url)
  }
  const imageUrl2 = (url) => {
    setImage2(url)
  }
  return (
    <ScrollView>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
        <Text >Image:{image}</Text>
        <Text >Image2:{image2}</Text>
        <UploadImage callback={imageUrl}/>
        <UploadImage callback={imageUrl2}/>
    </View>
    </ScrollView>
);}

export default App;
